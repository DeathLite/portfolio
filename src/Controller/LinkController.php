<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Form\Type\UpdateLinkFormType;
use App\Entity\Link;

class LinkController extends AbstractController
{
    protected $em;
    protected $userCurrent;

    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage) {
        $this->em = $entityManager;
        $this->userCurrent = $tokenStorage->getToken()->getUser();
    }

    /**
     * @Route("/link/update/{linkID}", name="link_update")
     */
    public function update(Request $request, $linkID)
    {

        if ( $linkID == 'new' ) {
            $link = new Link();
            $typeForm = "Ajouter";
        } else {

            $link = $this->em
                ->getRepository('App:Link')
                ->find($linkID);

            if ( !$link ) {
                $this->addFlash('danger', 'Ce lien n\'existe pas.');
                return $this->redirectToRoute('admin');
            }

            $typeForm = "Modifier";

        }

        $form = $this->createForm(UpdateLinkFormType::class, $link, Array("validation_groups" => "update"));
        $form->handleRequest($request);

        if($form->isSubmitted()) {

            $this->em->persist($link);
            $this->em->flush();

            if ( $typeForm == 'Ajouter' ){
                $this->addFlash('success', 'Le lien a bien été ajouté.');
            } else {
                $this->addFlash('success', 'Le lien a bien été mis à jour.');
            }

            return $this->redirectToRoute('admin');
        }

        return $this->render('updateLink.html.twig', Array(
            "link" => $link,
            "form" => $form->createView(),
            "typeForm" => $typeForm,
        ));

    }

}
