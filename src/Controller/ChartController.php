<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Form\Type\UpdateChartFormType;
use App\Form\Type\UpdateChartDataFormType;
use App\Entity\Chart;
use App\Entity\ChartData;

class ChartController extends AbstractController
{
    protected $em;
    protected $userCurrent;

    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage) {
        $this->em = $entityManager;
        $this->userCurrent = $tokenStorage->getToken()->getUser();
    }

    /**
     * @Route("/chart/update/{chartID}", name="chart_update")
     */
    public function update(Request $request, $chartID)
    {

        if ( $chartID == 'new' ) {
            $chart = new Chart;
            $typeForm = "Ajouter";
        } else {

            $chart = $this->em
                ->getRepository('App:Chart')
                ->find($chartID);

            if ( !$chart ) {
                $this->addFlash('danger', 'Ce graphique n\'existe pas.');
                return $this->redirectToRoute('admin');
            }

            $typeForm = "Modifier";

        }

        $form = $this->createForm(UpdateChartFormType::class, $chart, Array("validation_groups" => "update"));
        $form->handleRequest($request);

        $data = new ChartData();
        $formChartData = $this->createForm(UpdateChartDataFormType::class, $data, Array("validation_groups" => "update"));

        if($form->isSubmitted()) {

            $this->em->persist($chart);
            $this->em->flush();

            if ( $typeForm == 'Ajouter' ){
                $this->addFlash('success', 'Le graphique a bien été ajouté.');
            } else {
                $this->addFlash('success', 'Le graphique a bien été mis à jour.');
            }

            return $this->redirectToRoute('admin');
        }

        return $this->render('updateChart.html.twig', Array(
            "chart" => $chart,
            "chartsData" => $chart->getChartsData(),
            "form" => $form->createView(),
            "formChartData" => $formChartData->createView(),
            "typeForm" => $typeForm,
        ));

    }

    /**
     * @Route("/chart/{chartID}/add-data", name="chart_add_data")
     */
    public function addData(Request $request, $chartID){

        $chart = $this->em
            ->getRepository('App:Chart')
            ->find($chartID);

        if ( !$chart ) {
            $this->addFlash('danger', 'Ce graphique n\'existe pas.');
            return $this->redirectToRoute('admin');
        }

        $data = new ChartData();

        $form = $this->createForm(UpdateChartDataFormType::class, $data, Array("validation_groups" => "update"));
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $chart->addChartsData($data);

            $this->em->persist($data);
            $this->em->persist($chart);
            $this->em->flush();

            $this->addFlash('success', 'La donnée a bien été ajoutée au graphique.');

            return $this->redirectToRoute('chart_update', ['chartID' => $chart->getId()]);
        }

        return $this->redirectToRoute('chart_update', ['chartID' => $chart->getId()]);
    }

    /**
     * @Route("/chart/{chartID}/update-data/{dataID}", name="chart_update_data")
     */
    public function updateData(Request $request, $chartID, $dataID){

        $chart = $this->em
            ->getRepository('App:Chart')
            ->find($chartID);

        if ( !$chart ) {
            $this->addFlash('danger', 'Ce graphique n\'existe pas.');
            return $this->redirectToRoute('admin');
        }

        $chartData = $this->em
            ->getRepository('App:ChartData')
            ->find($dataID);

        if ( !$chartData ) {
            $this->addFlash('danger', 'Donnée introuvable.');
            return $this->redirectToRoute('chart_update', ['chartID' => $chart->getId()]);
        }

        $title = $request->get("title");
        $value = $request->get("value");

        if ( !$title || !$value ) {
            $this->addFlash('danger', 'Vous devez donner un titre et une valeur.');
            return $this->redirectToRoute('chart_update', ['chartID' => $chart->getId()]);
        }

        $chartData->setTitle($title);
        $chartData->setData($value);

        $this->em->persist($chartData);
        $this->em->flush();

        return $this->redirectToRoute('chart_update', ['chartID' => $chart->getId()]);
    }

    /**
     * @Route("/chart/{chartID}/delete-data/{dataID}", name="chart_delete_data")
     */
    public function deleteData(Request $request, $chartID, $dataID){

        $chart = $this->em
            ->getRepository('App:Chart')
            ->find($chartID);

        if ( !$chart ) {
            $this->addFlash('danger', 'Ce graphique n\'existe pas.');
            return $this->redirectToRoute('admin');
        }

        $chartData = $this->em
            ->getRepository('App:ChartData')
            ->find($dataID);

        if ( !$chartData ) {
            $this->addFlash('danger', 'Donnée introuvable.');
            return $this->redirectToRoute('chart_update', ['chartID' => $chart->getId()]);
        }

        $chart->removeChartsData($chartData);

        $this->em->remove($chartData);
        $this->em->persist($chart);
        $this->em->flush();

        $this->addFlash('success', 'La donnée a bien été supprimée du graphique.');

        return $this->redirectToRoute('chart_update', ['chartID' => $chart->getId()]);
    }

}
