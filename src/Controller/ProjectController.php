<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Form\Type\UpdateProjectFormType;
use App\Entity\Project;

class ProjectController extends AbstractController
{
    protected $em;
    protected $userCurrent;

    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage) {
        $this->em = $entityManager;
        $this->userCurrent = $tokenStorage->getToken()->getUser();
    }

    /**
     * @Route("/project/update/{projectID}", name="project_update")
     */
    public function update(Request $request, $projectID)
    {

        if ( $projectID == 'new' ) {
            $project = new Project;
            $typeForm = "Ajouter";
        } else {

            $project = $this->em
                ->getRepository('App:Project')
                ->find($projectID);

            if ( !$project ) {
                $this->addFlash('danger', 'Ce projet n\'existe pas.');
                return $this->redirectToRoute('admin');
            }

            $typeForm = "Modifier";

        }

        $form = $this->createForm(UpdateProjectFormType::class, $project, Array("validation_groups" => "update"));
        $form->handleRequest($request);

        if($form->isSubmitted()) {

            $this->em->persist($project);
            $this->em->flush();

            if ( $typeForm == 'Ajouter' ){
                $this->addFlash('success', 'Le projet a bien été ajouté.');
            } else {
                $this->addFlash('success', 'Le projet a bien été mis à jour.');
            }

            return $this->redirectToRoute('admin');
        }

        return $this->render('updateProject.html.twig', Array(
            "project" => $project,
            "form" => $form->createView(),
            "typeForm" => $typeForm,
        ));

    }

}
