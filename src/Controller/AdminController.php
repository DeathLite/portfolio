<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AdminController extends AbstractController
{
    protected $em;
    protected $userCurrent;

    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage) {
        $this->em = $entityManager;
        $this->userCurrent = $tokenStorage->getToken()->getUser();
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        $projects = $this->em
            ->getRepository('App:Project')
            ->findAll();

        $charts = $this->em
            ->getRepository('App:Chart')
            ->findAll();

        $links = $this->em
            ->getRepository('App:Link')
            ->findAll();

        return $this->render('admin.html.twig',
            [
                'projects' => $projects,
                'charts' => $charts,
                'links' => $links,
            ]);
    }

}
