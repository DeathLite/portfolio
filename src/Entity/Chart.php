<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="charts")
 */
class Chart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $labels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChartData", mappedBy="chart")
     */
    protected $chartsData;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $data;

    public function __construct()
    {
        $this->chartsData = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(?string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getLabels(): ?string
    {
        return $this->labels;
    }

    public function setLabels(?string $labels): self
    {
        $this->labels = $labels;

        return $this;
    }

    /**
     * @return Collection|ChartData[]
     */
    public function getChartsData(): Collection
    {
        return $this->chartsData;
    }

    public function addChartsData(ChartData $chartsData): self
    {
        if (!$this->chartsData->contains($chartsData)) {
            $this->chartsData[] = $chartsData;
            $chartsData->setChart($this);
        }

        return $this;
    }

    public function removeChartsData(ChartData $chartsData): self
    {
        if ($this->chartsData->contains($chartsData)) {
            $this->chartsData->removeElement($chartsData);
            // set the owning side to null (unless already changed)
            if ($chartsData->getChart() === $this) {
                $chartsData->setChart(null);
            }
        }

        return $this;
    }

    public function getChartsDataFormatted()
    {
        $chartsData = $this->chartsData;
        $array = [];

        foreach ( $chartsData as $cd ) {
            $array[] = $cd->getData();
        }

        return json_encode($array);
    }

    public function getChartsTitleFormatted()
    {
        $chartsData = $this->chartsData;
        $array = [];

        foreach ( $chartsData as $cd ) {
            $array[] = $cd->getTitle();
        }

        return json_encode($array);
    }

}
